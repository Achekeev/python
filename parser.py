import requests
from bs4 import BeautifulSoup
import csv
import subprocess


URL = 'https://auto.ria.com/newauto/marka-lifan/'
HEADERS = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                         '(KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36 OPR/70.0.3728.178', 'accept': '*/*'}

HOST = 'https://auto.ria.com'


def get_html(url, params=None):
    req = requests.get(url, headers=HEADERS, params=params)
    return req
FILE = 'cars.csv'

def get_pages(html):
    soup = BeautifulSoup(html, 'html.parser')
    pages = soup.find_all('span', class_='page-item mhide')
    if pages:
        return int(pages[-1].get_text())
    else:
        return 1


def get_content(html):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('div', class_='proposition')
    cars = []
    for item in items:
        cars.append({
                'title': item.find('h3', class_='proposition_name').get_text(strip=True),
                'link': HOST + item.find('a').get('href'),
                'price': item.find('div', class_='proposition_price').get_text(),
                'city': item.find('div', class_='proposition_region grey size13').get_text()
            })
    return cars


def save(items, path):
    with open(path, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(['Марка', 'Ссылка', 'Цена', 'Город, Фирма'])
        for item in items:
            writer.writerow([item['title'], item['link'], item['price'], item['city']])


def parse():
    URL = input('Enter Url: ')
    URL = URL.strip()
    html = get_html(URL)
    if html.status_code == 200:
        cars = []
        pages = get_pages(html.text)
        for page in range(1, pages+1):
            print(f'Страница {page} из {pages}...')
            html = get_html(URL, params={'page': page})
            cars.extend(get_content(html.text))
           # cars = get_content(html.text)
        save(cars, FILE)
        print(f'We have {len(cars)} cars')
        subprocess.Popen(FILE)
    else:
        print('Error')
parse()